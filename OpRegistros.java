/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bites;

/**
 *
 * @author hergusalap
 */
public class OpRegistros {

    public int temp[];

    public void sumaRRC(Registro a, RegistroCompleto b) {
        temp = new int[8];
        int acarreo = 0;
        int inicial = 0;
        for (int i = (a.registro.length - 1); i > -1; i--) {

            if ((b.b.registro[i] == 0 && a.registro[i] == 1) || (b.b.registro[i] == 1 && a.registro[i] == 0)) {
                if (acarreo > 0) {
                    acarreo = acarreo;
                    temp[i] = 0;
                } else {
                    acarreo = acarreo;
                    temp[i] = 1;
                }
            }
            if (b.b.registro[i] == 0 && a.registro[i] == 0) {
                if (acarreo > 0) {
                    acarreo--;
                    temp[i] = 1;
                } else {
                    temp[i] = 0;
                }
            }

            if ((b.b.registro[i] == 1 && a.registro[i] == 1)) {
                if (acarreo > 0) {
                    temp[i] = 1;
                    acarreo = acarreo;
                } else {
                    temp[i] = 0;
                    acarreo++;
                }
            }
        }
    }

    public void mover(Registro reg, Bites dato) {

        for (int i = 0; i < reg.registro.length; i++) {
            reg.registro[i] = dato.bite[i];
        }
    }

    public void imprimirRegistro(Registro reg) {
    System.out.println("Estado de registro ");
        for (int i = 0; i < reg.registro.length; i++) {
            System.out.print(reg.registro[i] + " ");
        }
        System.out.println("\n");
    }

    public static void main(String arg[]) {
        OpRegistros sp = new OpRegistros();
        Registro ah = new Registro(0, 1, 1, 0, 1, 1, 1, 1);
        Registro bl = new Registro(0, 1, 0, 1, 1, 0, 1, 1);
        Registro cl = new Registro(0, 0, 0, 0, 0, 0, 0, 0);
        Bites bite1 = new Bites(0, 0, 0, 0, 1, 0, 1, 0);

  //ystem.out.println(""+resultado);
        //sp.sumaRRC(a,p);
        System.out.println("\nRegistro resultado");
        ah.sumarRegistros(ah, bl);
        //sp.imprimirRegistro(ah);
        sp.imprimirRegistro(ah);
        sp.imprimirRegistro(bl);
        sp.imprimirRegistro(cl);
        sp.mover(bl, bite1);
        sp.imprimirRegistro(bl);
        sp.imprimirRegistro(cl);
        sp.mover(cl, bite1);
        sp.imprimirRegistro(cl);
    }

}
