/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bites;

/**
 *
 * @author hergusalap
 */
public class Registro {

    int registro[] = new int[8];
    int temp[] = new int[8];
    public Registro() {

    }

    public Registro(int a, int b, int c, int d, int e, int f, int g, int h) {
        registro[0] = a;
        registro[1] = b;
        registro[2] = c;
        registro[3] = d;
        registro[4] = e;
        registro[5] = f;
        registro[6] = g;
        registro[7] = h;
    }

    public boolean sumaRegistro(Registro a, Registro b) {
        temp = new int[8];
        Registro tempo = new Registro();
        int acarreo = 0;
        boolean error=false;
        int inicial = 0;
        for (int i = (a.registro.length - 1); i > -1; i--) {
            if(i==0&&acarreo>0){
                if(b.registro[i] == 1 || a.registro[i] == 1)
                error=true;
            }   
            if ((b.registro[i] == 0 && a.registro[i] == 1) || (b.registro[i] == 1 && a.registro[i] == 0)) {
                if (acarreo > 0) {
                    temp[i] = 0;
                    tempo.registro[i] = 0;
                } else {
                    temp[i] = 1;
                    tempo.registro[i] = 1;
                }
            }
            if (b.registro[i] == 0 && a.registro[i] == 0) {
                if (acarreo > 0) {
                    acarreo--;
                    temp[i] = 1;
                    tempo.registro[i] = 1;
                } else {
                    temp[i] = 0;
                    tempo.registro[i] = 0;
                }
            }

            if ((b.registro[i] == 1 && a.registro[i] == 1)) {
                //acarreo++;
                if (acarreo > 0) {
                    temp[i] = 1;
                    tempo.registro[i] = 1;
                    acarreo = acarreo;
                } else {
                    temp[i] = 0;
                    tempo.registro[i] = 0;
                    acarreo++;
                }
            }
        }
        if(error==false)
        {registro=temp;}
        return error;
    }
    public void sumarRegistros(Registro a,Registro b)
    {
        boolean resultado=sumaRegistro(a,b);
        if(resultado==false){
     a.imprimirD(a.temp);
  }
        
  if(resultado==true){
      System.out.println("No se puede llevar a cabo la operacion");
  }
    }
    public void imprimirD(int a[]) {
        for (int i =0 ; i <a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println("\n");
    }
}
